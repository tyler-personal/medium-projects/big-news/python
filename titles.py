from typing import List

unimportant_word_values = [
    'the', 'to', 'of', 'in', 'and', 'on', 'a', 'for', '-', 'at',
    'is', 'with', 'as', 'are', 'from', 'be', 'have', 'its', 'it', 'his',
    'by', '|', '...' 'about'
]


class Word:  # Case Sensitive by default
    def __init__(self, literal: str, title):
        self.value = literal

        self.important = literal not in unimportant_word_values
        self.count = 1
        self.titles: List[Title] = [title]

    def __str__(self):
        return self.value

    def add_occurrence(self, title):
        self.titles.append(title)
        self.count += 1


class Title:
    def __init__(self, literal_words: str, href: str):
        self.value = literal_words.strip().lower()
        self.href = href

        self.words: List[Word] = []
        self.importance: int = 0

    def __str__(self):
        return self.value


class TitleGroup:
    def __init__(self, titles: List[Title]):
        self.words = []
        self.titles = titles

        self.populate_words()
        self.set_title_importance_levels()

    def sorted(self):
        return sorted(self.titles, key=lambda t: t.importance, reverse=True)

    def populate_words(self):
        for title in self.titles:
            for literal in str(title).split():
                if literal in map(str, self.words):
                    for word in self.words:
                        if literal == str(word):
                            word.add_occurrence(title)
                else:
                    word = Word(literal, title)
                    self.words.append(word)
                    title.words.append(word)

    def set_title_importance_levels(self):
        for title in self.titles:
            title.importance = sum(word.count for word in title.words)/(len(title.words) + 1)
