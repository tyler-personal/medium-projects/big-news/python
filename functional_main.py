from typing import List
from collections import Counter
from collections import namedtuple

import requests
from toolz import *
from funcy import *

from requests import Session
from lxml import html

session: Session = requests.Session()
session.headers.update({'User-Agent': 'My User Agent'})

WebPage = namedtuple("WebPage", "url titles_xpath")


def retrieve_webpage_titles(webpage: WebPage) -> List[str]:
    page = session.get(webpage.url)
    tree = html.fromstring(page.content)

    return tree.xpath(webpage.titles_xpath)


def retrieve_multiple_webpage_titles(webpages: List[WebPage]) -> List[str]:
    return flatten(map(retrieve_webpage_titles, [reddit, google_news]))


def retrieve_words_from_titles(titles: List[str]) -> List[str]:
    return flatten(map(str.split, titles))


ignored_words = [
    'the', 'to', 'of', 'in', 'and', 'on', 'a', 'for', '-', 'at',
    'is', 'with', 'as', 'are', 'from', 'be', 'have', 'its', 'it', 'his',
    'by', '|', '...', 'about', 'that', 'can', 'i', 'now', 'says'
]


reddit = WebPage(url='https://reddit.com', titles_xpath='//a[contains(@class, "title")]/text()')
google_news = WebPage(url='https://news.google.com', titles_xpath='//a/text()')

words = pipe(
    [reddit, google_news],
    retrieve_multiple_webpage_titles,
    retrieve_words_from_titles,
    frequencies
)

print(words)

for word in words:
    print(str(words[word]) + ': ' + word)













