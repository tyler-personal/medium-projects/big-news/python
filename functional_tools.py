def select(func, iterable):
    return filter(func, iterable)


def reject(func, iterable):
    return filter(lambda x: not func(x), iterable)


def sort(func, iterable):
    return sorted(iterable, key=func, reverse=True)


def reverse_sort(func, iterable):
    return sorted(iterable, key=func, reverse=False)