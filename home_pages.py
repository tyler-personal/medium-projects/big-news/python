from abc import ABC

# Request Imports
from titles import *
import requests
from lxml import html

session = requests.Session()
session.headers.update({'User-Agent': 'My User Agent'})

words = [
    word for title in titles for word in title.split()
    if word.lower().strip() not in ignored_words
]

word_counter = Counter(words)
title_counter = Counter({title: sum(word_counter[word] for word in title) for title in titles})

list(map(lambda x: print(f'{x[0]} -- {x[1]}'), title_counter.most_common(15)))
class HomePage(ABC):
    def __init__(self):
        self.page = session.get(self.url)
        self.tree = html.fromstring(self.page.content)

        self.titles = self.tree.xpath(self.titles_xpath + '/text()')
        self.hrefs = self.tree.xpath(self.titles_xpath + '/@href')
        titles: List[Title] = [Title(text, href) for text, href in zip(self.titles, self.hrefs)]

        self.title_group: TitleGroup = TitleGroup(titles)

        super().__init__()

    def retrieve_filtered_words(self):
        ignored_words = [
            'the', 'to', 'of', 'in', 'and', 'on', 'a', 'for', '-', 'at',
            'is', 'with', 'as', 'are', 'from', 'be', 'have', 'its', 'it', 'his',
            'by', '|', '...', 'about', 'that', 'can', 'i', 'now', 'says'
        ]

        return [
            word for title in self.titles for word in title.split()
            if word.lower().strip() not in ignored_words
        ]


class Reddit(HomePage):
    def __init__(self):
        self.url = 'https://reddit.com'
        self.titles_xpath = '//a[contains(@class, "title")]'

        super().__init__()


class CNN(HomePage): # This seems to have some JS or something, really not working
    def __init__(self):
        self.url = 'http://cnn.com'
        self.titles_xpath = '//span'

        super().__init__()


class GoogleNews(HomePage):
    def __init__(self):
        self.url = 'https://news.google.com'
        self.titles_xpath = '//a'

        super().__init__()

